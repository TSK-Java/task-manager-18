package ru.tsc.kirillov.tm.api.service;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    List<User> findAll();

    User add(User user);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

}
