package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public String getDescription() {
        return "Обновить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(NumberUtil.fixIndex(index), name, description);
    }

}
