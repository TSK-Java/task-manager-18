package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "change-password";
    }

    @Override
    public String getDescription() {
        return "Изменение пароля пользователя.";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[Изменение пароля пользователя]");
        System.out.println("Введите новый пароль");
        final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(userId, newPassword);
    }

}
