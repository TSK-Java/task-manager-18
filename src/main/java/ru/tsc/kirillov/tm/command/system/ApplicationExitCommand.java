package ru.tsc.kirillov.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Закрытие приложения.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
