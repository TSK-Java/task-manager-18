package ru.tsc.kirillov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("Ошибка! Описание не задано.");
    }

}
