package ru.tsc.kirillov.tm.exception.system.value;

import ru.tsc.kirillov.tm.exception.AbstractException;

public abstract class AbstractValueException extends AbstractException {

    public AbstractValueException() {
    }

    public AbstractValueException(String message) {
        super(message);
    }

    public AbstractValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractValueException(Throwable cause) {
        super(cause);
    }

    public AbstractValueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
