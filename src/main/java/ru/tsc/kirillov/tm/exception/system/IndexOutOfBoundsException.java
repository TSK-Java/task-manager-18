package ru.tsc.kirillov.tm.exception.system;

public class IndexOutOfBoundsException extends java.lang.IndexOutOfBoundsException {

    public IndexOutOfBoundsException() {
        this("Ошибка! Индекс вышел за допустимые пределы.");
    }

    public IndexOutOfBoundsException(String s) {
        super(s);
    }

}
